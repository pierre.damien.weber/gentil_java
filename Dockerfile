FROM openjdk:8
WORKDIR /
EXPOSE 8080
ADD target/simple-app-0.1-SNAPSHOT.jar ./monapp.jar
CMD ["java","-jar","monapp.jar"]
